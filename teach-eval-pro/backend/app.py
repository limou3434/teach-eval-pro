from flask import Flask, render_template
from pathlib import Path


work_path = Path.cwd() # 工作路径
frontend_path = work_path / 'frontend' # 前端路径

print(frontend_path)

app = Flask(
    __name__,
    template_folder=str(frontend_path / 'templates'), # 模板文件
    static_folder=str(frontend_path / 'static') # 其他静态文件
)

# 路由定义
@app.route('/')
def login():
    return render_template('login.html')


if __name__ == '__main__':
    app.run(debug=True)
