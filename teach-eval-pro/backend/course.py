""" 文件描述
2024-3-6-ljp: 描述了一个课程类结构
"""

import openpyxl as xl
from student import Student

class Course(object):
    """概述:该类可以描述一个课程的信息
    属性:...
    """
    def __init__(
            self, course_name,
            course_size,
            course_time,
            course_teach,
            course_credit,
            course_credit_hour,

            attendance_full_mark,
            homework_and_test_full_mark,
            midterm_test_full_mark,
            final_grade_full_mark,
            final_proportion,
            course_target
            ) -> None:
        # 1.班级基本信息
        self.course_name = course_name
        self.course_size = course_size
        self.course_time = course_time
        self.course_teach = course_teach
        self.course_credit = course_credit
        self.course_credit_hour = course_credit_hour
        self.course_target = course_target

        # 2.分数百分比/分数满分值
        self.attendance_full_mark = attendance_full_mark
        self.homework_and_test_full_mark = homework_and_test_full_mark
        self.midterm_test_full_mark = midterm_test_full_mark
        self.final_grade_full_mark = final_grade_full_mark
        self.final_proportion = final_proportion
        
        # 3.学生列表和学生字典
        self.course_students_dict = {}
        self.course_students_list = []

    def score_entry_import(self, data_workbook_path) -> None:
        """作用:读取成绩表中的数据填充到学生字典中的字段"""
        data_workbook = xl.load_workbook(str(data_workbook_path), data_only=True)
        achievement_summary_sheet = data_workbook.active

        for row in achievement_summary_sheet.iter_rows(min_row=2, max_row=self.course_size+1):
            # 1.创建一个学生对象
            s = Student()

            # 2.填写相关字段
            s.name = row[0].value
            s.id = row[1].value
            s.specialty = row[2].value
            s.class_and_grade = row[3].value
            s.attendance_score = row[4].value
            s.homework_and_test_score = row[5].value
            s.midterm_test_score = row[6].value
            s.final_grade_score[0], s.final_grade_score[1], s.final_grade_score[2] = row[7].value, row[8].value, row[9].value

            # 3.添加进班级学生名单
            self.course_students_dict[s.id] = s # 推送学生个体进入字典,并且可以根据 id 查找到该学生
            self.course_students_list.append(s) # 方便使用列表来操作每一个学生