""" 文件描述
2024-3-6-ljp: main.py 是整个项目的启动程序, 如果需要进行开发请从这里开始看起
"""
from pathlib import Path # 导入 Pata 类
from course import Course # 导入 Course 类
from draw import Draw # 导入 Draw 类
from result import create_final_result, create_attribute_table, create_grade_table
import os

# 获取当前工作目录
current_working_directory = Path.cwd() / 'backend'  # D:\GitWork\teach-eval-pro\teach-eval-pro\backend
print(current_working_directory)

# 课程负责人创建班级对象
cou = Course(
    course_name='高级语言程序设计',
    course_size=67,
    course_time='周二3-5节',
    course_teach='苑俊英',
    course_credit=2,
    course_credit_hour=36,
    course_target=[
        "考查学生掌握C语言基本语法及基本技能",
        "课程目标2主要考查学生通过分析实际问题，并编写程序解决该问题的能力",
        "课程目标3主要培养学生自主学习的意识，并能够通过归纳、总结完成课后作业"
        ],

    attendance_full_mark=(8, 0, 2),
    homework_and_test_full_mark=(20 , 5, 5),
    midterm_test_full_mark=(8, 2, 0),
    final_grade_full_mark=(40, 10, 0),
    final_proportion=0.5
)

# TODU-2024-3-9-ljp:将内存中的课程属性填入数据库中

# 根据课程属性创建属性表格，供课程负责人查看
print(current_working_directory)


create_attribute_table(cou_obj=cou, save_path=current_working_directory / 'resource' / 'attribute_table.xlsx')
os.startfile(str(current_working_directory / 'resource' / 'attribute_table.xlsx'))

# 根据属性和其他字段，创建对应的成绩单表格
create_grade_table(cou_obj=cou, save_path=current_working_directory / 'resource' / 'grade_table.xlsx')
os.startfile(str(current_working_directory / 'resource' / 'grade_table.xlsx'))

# TODU-2024-3-9-ljp:任课教师根据课程属性要求，填写学生成绩传递到数据库

# 从数据库中读取成绩数据到课程对象中
cou.score_entry_import(data_workbook_path=current_working_directory / 'resource' / 'grade_table.xlsx')

# 收集分析过程中必要的数据
datas = {} # {id:(目标一达成度,目标二达成度,目标三达成度), ...}

for student in cou.course_students_list:
    # 创建元组存储学生的各个目标的达成度
    goals_progress = []
    
    # 每个目标达成度的总计算
    for i in range(0, 3):
        progress = (
            (student.attendance_score * cou.attendance_full_mark[i] / 100  
            + student.homework_and_test_score * cou.homework_and_test_full_mark[i] / 100 
            + student.midterm_test_score * cou.midterm_test_full_mark[i] / 100 
            + student.final_grade_score[i] * cou.final_proportion) / (cou.attendance_full_mark[i] + cou.homework_and_test_full_mark[i] + cou.midterm_test_full_mark[i] + cou.final_grade_full_mark[i])
        )
        goals_progress.append(progress)

    # 构建达成度数据块
    datas[student.id] = goals_progress

# 8.绘制分析图
ana = Draw(save=True, save_path=current_working_directory / "resource") # 设置为保存,保存路径为默认

# 8.1.绘制矩形图
progress_1_average_score = 0
progress_2_average_score = 0
progress_3_average_score = 0
for student_data in datas.values(): # datas = {id:(目标一达成度,目标二达成度,目标三达成度), ...}
    progress_1_average_score += student_data[0]
    progress_2_average_score += student_data[1]
    progress_3_average_score += student_data[2]

progress_1_average_score = (progress_1_average_score / cou.course_size)
progress_2_average_score = (progress_2_average_score / cou.course_size)
progress_3_average_score = (progress_3_average_score / cou.course_size)

ana.bar_charts(['目标1', '目标2', '目标3'], [progress_1_average_score, progress_2_average_score, progress_3_average_score], 'goal_achievement_bar_chart', '目标', '达成度值')

# 8.2.绘制散点图
progress_1 = []
progress_2 = []
progress_3 = []
for data in datas.values():
    progress_1.append(data[0])
    progress_2.append(data[1])
    progress_3.append(data[2])

ana.scatter_charts(range(1, cou.course_size + 1), progress_1, 'target_1_scatter_diagram', '学生', '达成度值')
ana.scatter_charts(range(1, cou.course_size + 1), progress_2, 'target_2_scatter_diagram', '学生', '达成度值')
ana.scatter_charts(range(1, cou.course_size + 1), progress_3, 'target_3_scatter_diagram', '学生', '达成度值')
# ana.charts_show() # debug

# 9.得到分析结果
head = f"{cou.course_name} 课程分析结果"

paragraphs = [f"{cou.course_name}包含{len(cou.course_target)}个目标，根据本课程的考核结果，计算每个课程目标的达成度", f"计算每个学生各项课程目标的达成度，对{cou.course_name}课程{cou.course_size}位学生进行作图，得到各个课程目标的达成度分布图："]
paragraphs.append(current_working_directory / "resource" / "goal_achievement_bar_chart.png")

for index, tar in enumerate(cou.course_target):
    paragraphs.append(f"得到课程目标{index+1}的达成度分布图：")
    paragraphs.append(current_working_directory / "resource" / f"target_{index+1}_scatter_diagram.png")

create_final_result(head, paragraphs, current_working_directory / 'resource' / 'final_result.docx')

# 10.TODU-2024-3-9-ljp:打包程序结果
os.startfile(str(current_working_directory / 'resource' / 'final_result.docx'))