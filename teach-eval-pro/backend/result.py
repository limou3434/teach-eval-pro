""" 文件描述
2024-3-6-ljp: result.py 是为了得到项目结果的一个文件
"""

import docx # 导入 Word 文档类
import openpyxl as xl # 导入 Excel 表格类
from pathlib import Path # 导入 Pata 类



def create_attribute_table(cou_obj, save_path) -> None:
    """作用: 生成一个课程属性表"""
    # 检查文件是否存在
    if save_path.exists(): # 存在
        return

    # 创建一个 Workbook 对象
    workbook = xl.Workbook()

    # 填写工作表名
    sheet = workbook.active
    sheet.title = f"{cou_obj.course_name}属性表"

    # 合并单元格并且赋予标题
    sheet.merge_cells(start_row=1, start_column=1, end_row=1, end_column=1+len(cou_obj.course_target))
    sheet.cell(row=1, column=1, value=sheet.title)

    # 填充行字段
    row_filed = ['项目', '分数']
    for i in range(1, len(cou_obj.course_target)+1):
        row_filed.append(f"课程目标{i}")
    for index, field in enumerate(row_filed):
        sheet.cell(row=2, column=index+1, value=field)

    # 填充列字段
    col_filed = ['课堂考勤与讨论', '平时作业/小测', '期中考试', '期末考试']
    for index, field in enumerate(col_filed):
        sheet.cell(row=index+3, column=1, value=field)

    # 填充单元值
    sheet.cell(row=3, column=2, value=sum(cou_obj.attendance_full_mark))
    for i in range(0, len(cou_obj.attendance_full_mark)):
        sheet.cell(row=3, column=i+3, value=cou_obj.attendance_full_mark[i])

    sheet.cell(row=4, column=2, value=sum(cou_obj.homework_and_test_full_mark))
    for i in range(0, len(cou_obj.attendance_full_mark)):
        sheet.cell(row=4, column=i+3, value=cou_obj.homework_and_test_full_mark[i])

    sheet.cell(row=5, column=2, value=sum(cou_obj.midterm_test_full_mark))
    for i in range(0, len(cou_obj.attendance_full_mark)):
        sheet.cell(row=5, column=i+3, value=cou_obj.midterm_test_full_mark[i])

    sheet.cell(row=6, column=2, value=sum(cou_obj.final_grade_full_mark))
    for i in range(0, len(cou_obj.attendance_full_mark)):
        sheet.cell(row=6, column=i+3, value=cou_obj.final_grade_full_mark[i])

    # 保存表格文件
    workbook.save(str(save_path))

def create_grade_table(cou_obj, save_path) -> None:
    """作用: 生成一个学生成绩表"""
    # 检查文件是否存在
    if save_path.exists():
        return

    # 创建一个 Workbook 对象
    workbook = xl.Workbook()

    # 填写工作表名
    sheet = workbook.active
    sheet.title = "学生成绩表"
    
    # 填充字段
    fileds = ['姓名', '学号', '专业', '年级与班级', '考勤', '作业+小测', '期中作业']
    for i in range(1, len(cou_obj.course_target)+1):
        fileds.append(f"期末目标{i}得分")
    for index, filed in enumerate(fileds):
        sheet.cell(row=1, column=index+1, value=filed)
        
    workbook.save(str(save_path))

def create_final_result(head, paragraphs, save_path) -> None:
    """作用: 根据图文列表生成一个 word 文档"""
    doc = docx.Document() # 创建一个文档类

    doc.add_heading(head, 1) # 添加文档标题

    for paragraph in paragraphs: # 不断添加段落
        # 如果是 Path 对象,且对应路径存在，且为png图片
        if isinstance(paragraph, Path) and paragraph.exists() and paragraph.suffix == ".png":
            doc.add_picture(str(paragraph))
        else: # 否则就是对应文本
            doc.add_paragraph(paragraph)

    doc.save(str(save_path)) # 保存文档