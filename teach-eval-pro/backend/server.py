""" 文件描述
使用 Flask 框架实现的后端服务
"""
from flask import Flask, request, jsonify, redirect, url_for # Flask 是一个用于构建 Web 应用的微型框架，request 用于处理客户端请求，jsonify 用于将 Python 对象转换为 JSON 格式
from flask_cors import CORS
import logging
import sql_operation
from pathlib import Path # 导入 Pata 类

current_working_directory = Path.cwd()

# 配置日志记录器
# logging.basicConfig(filename='./server.log', level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(filename)s - line %(lineno)d - %(message)s')
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(filename)s - line %(lineno)d - %(message)s')

# 创建应用
app = Flask(__name__) # 创建 Flask 应用对象，__name__ 为当前模块的名称
CORS(app)  # 添加跨域请求处理，前端可以从不同域名或端口向后端发送跨域请求

# 登录页面处理逻辑
@app.route('/login', methods=['POST']) # 使用 @app.route 装饰器定义路由 /submit-login，并指定只接受 POST 请求
def login():
    logging.debug('The server init succeed')

    # 获取 POST 请求中的数据
    username = request.form.get('username')
    password = request.form.get('password')
    verification_code = request.form.get('verification_code')

    # 验证用户名密码
    with sql_operation.SqlOperation() as sqlo:
        if sqlo.login(username, password):
            # return jsonify('欢迎')
            return redirect(url_for('welcome'))
        else:
            return jsonify('无该用户/密码错误')

# 跳转内部页面
@app.route("/welcome")
def welcome():
    with open(str(current_working_directory / ""), 'r') as file:
        welcome_page = file.read()
    return welcome_page

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3611) # 接受任意 ip 有关 3611 的请求
