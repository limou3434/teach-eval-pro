"""
描述: 主要是对 MySQL 的操作的封装
"""
import mysql.connector
import os
import bcrypt

class SqlOperation:
    __mysql_config = {
        'host': os.getenv('EIMOU_DATABASE_HOST'),
        'user': os.getenv('EIMOU_DATABASE_USER'),
        'password': os.getenv('EIMOU_DATABASE_PASSWORD'),
        'database': os.getenv('EIMOU_DATABASE_NAME')
    }

    def __enter__(self):
        self.__db_connection = mysql.connector.connect(**self.__mysql_config) # 连接数据库
        self.__cursor = self.__db_connection.cursor() # 初始一个可操作对象
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if self.__cursor is not None:
            self.__cursor.close()
        if self.__db_connection is not None:
            self.__db_connection.close()

    def modify(self, sql):
        """
        作用: 修改数据表
        参数: sql 代表一条语句
        返回: None
        """
        self.__cursor.execute(sql) # 执行 sql 语句
        self.__db_connection.commit() # 提交事务

    def select(self, sql) -> dict:
        """
        作用: 查询数据表
        参数: sql 代表一条语句
        返回: result 元组列表, 内含查询的数据, 若无结果返回 None
        """
        self.__cursor.execute(sql) # 执行 sql 语句
        result = self.__cursor.fetchall()
        if len(result) != 0:
            columns = [desc[0] for desc in self.__cursor.description]  # 获取查询结果的列名
            result_dict = [dict(zip(columns, row)) for row in result][0]  # 将结果转换为字典列表
            return result_dict
        else:
            return None

    def login(self, user_id, password):
        """
        作用: 查验密码是否正确
        参数: user_id, password: 分别对应用户的 id, 密码
        返回: bool 值, 可以判断密码是否和数据库中的密码匹配
        """
        user_info = self.select(f'select * from users where user_id={user_id};')  # 获取数据库中对应用户的信息
        if user_info: # 如果存在该用户
            hashed_password = user_info['password'].encode('utf-8')  # 字符串转换为字节序列
            if bcrypt.checkpw(password.encode('utf-8'), hashed_password):  # 比较密码是否相同
                return True
            else:
                return False
        else: # 如果不存在该用户
            return None

if __name__ == '__main__':
    with SqlOperation() as sqlo:
        print(sqlo.select('select * from users'))
        print(sqlo.login('2210330038', '31415926'))
        print(sqlo.login('2210330038', '31415923'))
