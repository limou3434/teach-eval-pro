""" 文件描述
2024-3-6-ljp: 描述了一个学生类结构
"""

class Student(object):
    """概述:该类可以描述一个学生的信息
    属性:...
    """
    def __init__(
            self,
            name='No Name',
            id='No id',
            specialty='No specialty',
            class_and_grade='No class and grade'
        ) -> None:
        # 1.学生基本信息
        self.name = name
        self.id = id
        self.specialty = specialty
        self.class_and_grade = class_and_grade

        # 2.学生成绩信息
        self.attendance_score = 0
        self.homework_and_test_score = 0
        self.midterm_test_score = 0
        self.final_grade_score = [0, 0, 0]