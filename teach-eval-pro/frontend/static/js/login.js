// 通过 JS 监听表单的提交事件，当用户点击登录按钮提交表单时，会执行这个匿名函数内的逻辑（Fetch API 是 Web 开发中用于发送和接收网络请求的现代 JS API）
document.getElementById('login-form').addEventListener('submit', function(event) { //获取 id='login-form' 的 <form> 表单元素，并为其添加了一个 submit 提交事件监听器，当用户提交表单时，会触发这个匿名回调函数内的逻辑，并且传递一个 event 事件对象
    event.preventDefault(); // 阻止表单的默认提交行为，这样可以通过 JS 来处理表单的提交操作，而不是让浏览器自动提交 

    // 提取表单数据
    var formData = new FormData(event.target); // 将表单数据封装成一个键值对的集合

    // 发送 POST 请求到指定服务器
    fetch('http://localhost:3611/login', { // fetch 就是一个 Promise 对象
        method: 'POST',
        body: formData
    })
    .then(response => response.json()) //处理服务器返回的响应数据，将响应体解析为 JSON 格式
    .then(data => {
        console.log(data); // 返回控制台日志
        var confirmation = confirm(JSON.stringify(data)); // 显示确认弹窗
        if (confirmation) {
            // 用户点击确认按钮后的处理逻辑
            console.log('User confirmed.');
        } else {
            // 用户点击取消按钮后的处理逻辑
            console.log('User cancelled.');
        }
    })
    .catch(error => { //整个 Promise 链中任何一个 Promise 返回错误或被拒绝，则会触发 .catch()
        console.error('Error:', error);
    });
});